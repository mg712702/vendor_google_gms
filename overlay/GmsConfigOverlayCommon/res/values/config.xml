<?xml version="1.0" encoding="utf-8"?>
<resources>

    <!-- For Factory Reset Protection -->
    <string name="config_persistentDataPackageName" translatable="false">com.google.android.gms</string>

    <!-- Enable doze mode -->
    <bool name="config_enableAutoPowerModes">true</bool>

    <!-- Enable overlay for all location components. -->
    <bool name="config_enableNetworkLocationOverlay" translatable="false">true</bool>
    <bool name="config_enableFusedLocationOverlay" translatable="false">true</bool>
    <bool name="config_enableGeocoderOverlay" translatable="false">true</bool>
    <bool name="config_enableGeofenceOverlay" translatable="false">true</bool>

    <!-- Sets the package names whose certificates should be used to
         verify location providers are allowed to be loaded. -->
    <string-array name="config_locationProviderPackageNames" translatable="false">
        <item>com.google.android.gms</item>
        <item>com.android.location.fused</item>
    </string-array>

    <!-- Sets the package name for location extra packages -->
    <string-array name="config_locationExtraPackageNames" translatable="false">
         <item>com.google.android.gms.location.history</item>
    </string-array>

    <!-- Sets the Ephemeral Resolver [DO NOT TRANSLATE] -->
    <string-array name="config_ephemeralResolverPackage" translatable="false">
        <item>com.google.android.gms</item>
    </string-array>

    <!-- The package name of the default network recommendation app. -->
    <string name="config_defaultNetworkRecommendationProviderPackage" translatable="false">com.google.android.gms</string>

    <!-- Default service to enable with accessibility shortcut [DO NOT TRANSLATE] -->
    <string name="config_defaultAccessibilityService" translatable="false">com.google.android.marvin.talkback/.TalkBackService</string>

    <!-- Default autofill service to enable [DO NOT TRANSLATE] -->
    <string name="config_defaultAutofillService" translatable="false">com.google.android.gms/.autofill.service.AutofillService</string>

    <string-array name="config_integrityRuleProviderPackages" translatable="false">
        <item>com.android.vending</item>
        <item>com.google.android.gms</item>
    </string-array>

    <!-- The set of system packages on device that are queryable regardless of the contents of their
         manifest. -->
    <string-array name="config_forceQueryablePackages" translatable="false">
        <item>com.android.settings</item>
        <item>com.google.android.gms</item> <!-- STOPSHIP(b/147699130) -->
        <item>com.android.vending</item> <!-- STOPSHIP(b/147699130) -->
    </string-array>

    <!-- An array of packages for which notifications cannot be blocked. -->
    <string-array name="config_nonBlockableNotificationPackages" translatable="false">
        <item>com.google.android.setupwizard</item>
        <item>com.google.android.apps.restore</item>
        <item>com.google.android.dialer</item>
    </string-array>

    <!-- An array of packages that can make sound on the ringer stream in priority-only DND mode -->
    <string-array name="config_priorityOnlyDndExemptPackages" translatable="false">
        <item>com.google.android.dialer</item>
        <item>android</item>
        <item>com.android.systemui</item>
    </string-array>

    <!-- The package name for the default wellbeing application. [DO NOT TRANSLATE] -->
    <string name="config_defaultWellbeingPackage" translatable="false">com.google.android.apps.wellbeing</string>

    <!-- The component to be the default supervisor profile owner [DO NOT TRANSLATE] -->
    <string name="config_defaultSupervisionProfileOwnerComponent" translatable="false">com.google.android.gms/.kids.account.receiver.ProfileOwnerReceiver</string>

    <!-- Whether to only install system packages on a user if they're whitelisted for that user
         type. These are flags and can be freely combined.
         0  - disable whitelist (install all system packages; no logging)
         1  - enforce (only install system packages if they are whitelisted)
         2  - log (log non-whitelisted packages)
         4  - any package not mentioned in the whitelist file is implicitly whitelisted on all users
         8  - same as 4, but just for the SYSTEM user
         16 - ignore OTAs (don't install system packages during OTAs)
         Common scenarios:
          - to enable feature (fully enforced) for a complete whitelist: 1
          - to enable feature for an incomplete whitelist (so use implicit whitelist mode): 5
          - to enable feature but implicitly whitelist for SYSTEM user to ease local development: 9
          - to disable feature completely if it had never been enabled: 16
          - to henceforth disable feature and try to undo its previous effects: 0
        Note: This list must be kept current with PACKAGE_WHITELIST_MODE_PROP in
        frameworks/base/services/core/java/com/android/server/pm/UserSystemPackageInstaller.java -->
    <integer name="config_userTypePackageWhitelistMode">5</integer> <!-- 1+4+8 -->

    <!-- Component name that accepts ACTION_SEND intents for nearby (proximity-based) sharing.
         Used by ChooserActivity. -->
    <string name="config_defaultNearbySharingComponent" translatable="false">com.google.android.gms/com.google.android.gms.nearby.sharing.ShareSheetActivity</string>

    <!-- The package name for the system's app prediction service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.intelligence/.AppPredictionService"
    -->
    <string name="config_defaultAppPredictionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiPredictionService</string>

    <!-- The component name for the default system attention service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         See android.attention.AttentionManagerService.
    -->
    <string name="config_defaultAttentionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.attention.service.AiAiAttentionService</string>

    <!-- The package name for the system's augmented autofill service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, augmented autofill wil be
         disabled.
         Example: "com.android.augmentedautofill/.AugmentedAutofillService"
    -->
    <string name="config_defaultAugmentedAutofillService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAugmentedAutofillService</string>

    <!-- The package name for the system's content capture service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content capture will be
         disabled.
         Example: "com.android.contentcapture/.ContentcaptureService"
    -->
    <string name="config_defaultContentCaptureService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentCaptureService</string>

    <!-- The package name for the system's content suggestions service.
         Provides suggestions for text and image selection regions in snapshots of apps and should
         be able to classify the type of entities in those selections.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content suggestions wil be
         disabled.
         Example: "com.android.contentsuggestions/.ContentSuggestionsService"
    -->
    <string name="config_defaultContentSuggestionsService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentSuggestionsService</string>

    <!-- Colon separated list of package names that should be granted DND access -->
    <string name="config_defaultDndAccessPackages" translatable="false">com.google.android.apps.wellbeing:com.google.android.settings.intelligence</string>

    <!-- Colon separated list of package names that should be granted Notification Listener access -->
    <string name="config_defaultListenerAccessPackages" translatable="false">com.android.launcher3:com.google.android.setupwizard:com.google.android.apps.restore:com.google.android.as:com.google.android.projection.gearhead</string>

    <!-- The component name for the system-wide captions manager service.
         This service must be trusted, as the system binds to it and keeps it running.
         Example: "com.android.captions/.SystemCaptionsManagerService"
    -->
    <string name="config_defaultSystemCaptionsManagerService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.captions.SystemCaptionsManagerService</string>

    <!-- The component name for the system-wide captions service.
         This service must be trusted, as it controls part of the UI of the volume bar.
         Example: "com.android.captions/.SystemCaptionsService"
    -->
    <string name="config_defaultSystemCaptionsService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.captions.CaptionsService</string>

    <!-- The package name for the default system textclassifier service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.textclassifier"
         If no textclassifier service with the specified name exists on the device (or if this is
         set to empty string), a default textclassifier will be loaded in the calling app's process.
         See android.view.textclassifier.TextClassificationManager.
    -->
    <string name="config_defaultTextClassifierPackage" translatable="false">com.google.android.as</string>

    <!-- The amount to scale fullscreen snapshots for Overview and snapshot starting windows. -->
    <item type="dimen" name="config_highResTaskSnapshotScale">0.8</item>

    <!-- The app which will handle routine based automatic battery saver, if empty the UI for
         routine based battery saver will be hidden -->
    <string name="config_batterySaverScheduleProvider">com.google.android.apps.turbo</string>

    <!-- Package name that will receive an explicit manifest broadcast for
         android.os.action.POWER_SAVE_MODE_CHANGED. -->
    <string name="config_powerSaveModeChangedListenerPackage">com.google.android.flipendo</string>

</resources>
